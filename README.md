
### HEROKU PUSH ###

* heroku login (email/pwd)
* heroku create
* git remote add heroku <heroku URL> 
* git remote -v
* git add . 
* git commit -m “push to heroku”
* git push heroku master 

### HEROKU NEW APP ###

* git remote remove heroku
* git remote add heroku  <NEW heroku URL>
* git remote -v
* git add . 
* git commit -m “push to heroku”
* git push heroku master 

/**
 * Client side app.controller.js
 * AngularJS
 */

"use strict";

 (function() { //IIFE

     angular.module("PopQuizApp").controller("PopQuizCtrl", PopQuizCtrl); //no more []

     // any communication is through $http (passing from front end to back end)
     PopQuizCtrl.$inject = ["$http"]; // [] is now here (dependency injection)
     
     function PopQuizCtrl($http) { //this function is actually a class 
        
        var self = this; //or to be clearer use var selfPopQuizCtrl 

        self.qnNum = 1; 
        self.numQuestions = 0; //initialize number of questions (don't hardcode to 5)
                               //initialize at 0

        self.quiz = { //initialize quiz object 

        };

        self.finalAnswer = {
            id: 0,
            value: "",
            comments: "",
            message: "",
        }

        
        self.initForm = function () {
    
            $http.get("/numQuestions")
            .then(function(result) { 
                    console.log(result);
                    self.numQuestions = result.data; //this comes from app.js, under app.get("/numQuestions") quizes.LENGTH
                }).catch(function (e) {
                    console.log(e);
                });
    
                $http.get("/popquizes")
                    .then(function (result) {
                        console.log(result);
                        self.quiz = result.data;
                    }).catch(function (e) {
                        console.log(e);
                    });
            };

            self.initForm(); //initialize the form 

            self.submitQuiz = function () {

                console.log("submitQuiz");
                self.finalanswer.id = self.quiz.id;
                $http.post("/submit-quiz", self.finalanswer)
                    .then(function (result) { //result and .json(quiz) is the same
                        console.log(result); 
                        if (result.data.isCorrect) { //check boolean if it's true or false
                            self.finalanswer.message = "It's CORRECT !";
                        } else {
                            self.finalanswer.message = "WRONG !";
                        }
    
                        //if the following is received from the server,
                        //the client has to then show the End Quiz button
                        if(result.data.endOfQuiz) {
                            console.log("End of quiz, score = " + result.data.score);
                            self.endOfQuiz = true;
                            self.score =  result.data.score;
                        }
                    }).catch(function (e) {
                        console.log(e);
                    });
    
                self.submitDisabled = true; //disable the Submit button. 
            };

        //this is the function invoked when the user click on the Next button
        //it is very similar to the initForm function
        //it will use the same endpoint /popquizes to get a question
        //the differences are that it has some additional logic to enable/disable the buttons and display additional info
            self.getNext = function () {
                self.finalanswer.message = ""; //hide the status message
                self.finalanswer.value = ""; //deselect the radio button selection from previous choice
                self.finalanswer.comments = ""; //remove comments

                self.submitDisabled = false; //enable the Submit button (for the next question)

            $http.get("/popquizes") //same popquiz end point
            .then(function (result) {
                console.log(result);
                self.quiz = result.data;
                self.qnNum++; //increase the question serial number
            }).catch(function (e) {
                console.log(e);
            });
        };

        //to toggle the indicator which will then hide the question/answers screen and show the result screen instead
        self.EndQuiz = function () {
            self.showResult = true; //to show 
        }

    }

     //end controller


 })();